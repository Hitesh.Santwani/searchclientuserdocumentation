# Claims Application 
The claims application serves as a supplementary application to our post fixture project, claims are results of breakage of contracts set at the begining of the voyage. Each one of these breakages of contracts can encur charges which have to placed through a claims; so a post fixture can have many claims associated with it. Each one of these claims has its own timeline, documentation and emails associate with it. This can become cumbersome when trying to keeps all of these items organized; luckily each claim is indivaully listed on every post fixtures and also is also searchable from the search clients main page.
## Claims Details

![alt text](./media/ClaimsDetails.png "Title")  

Apon loading an indivual claims page; the claims detail page will load first. This page contains some of the more discreiptive informatiopn about the claim, such as it's last status update description and date. Below this, a card containing the descriptive details of the claim are displayed, who made the claim, whom the claim was made against, the claim number, the claim type, the claim amount and and additional claim details.
* Addition claim details can be added by clicking on the edit button at the base of the card.

Next to be displayed are the Timebar details associated with the post fixture this claim is attached to. If the expriation date of the timebar is quickly coming to a close, the text will be displayed in red.

Finally, all of the attached documentation associated with the current claims are displayed in table at the bottom of the page. Most of this documentation is also located in the claims folder associated with 