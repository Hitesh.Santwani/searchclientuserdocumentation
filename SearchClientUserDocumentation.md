# Search Client User Documentation
### Performing a Search
The Mcquilling Search Client contains 5 separate dashboards that allow users to query McQuilling's proprietary MLISA database.
* Fixtures
* Ports
* Claims
* Vessels
* Companies

Each dashboard contains a form that contains multiple fields related to each data set, this form is placed at the header of each page adjacent to two buttons; one button to perform the search and one to navigate to another search dashboard. All a user must do to query the McQuilling MLISA database is simply select the data type they would like to search, then enter the parameters for the search in the corresponding form.

![alt text](./media/SearchBar.png "Title")
### Viewing Search Results
A loading animation will appear on the screen as the client requests the MLISA API, once the request is completed a paginated display will appear with the first 20 results from your search. To navigate forward or backward within your search results; move to either the bottom or the top of the returned results. Once there, a Previous and Next Page button will be displayed, as well as your current page position and the total pages returned.

![alt text](./media/SearchResults1.png "Title")

Each result will appear as a row, once the row is clicked on the search client performs a secondary search to populate all necessary data about the search result chosen. The row will expand and show all of the collected data to the user, note that the data is separated into a tabular format. Each tab will contain a certain type of data that is connected to the given record, this is meant to be a brief overview of the record and not meant to encompass all information about this item.

 ![alt text](./media/SearchResults2.png "Title")

If the user wishes to see a more complete set of data connected to a record, every row returned by the search contains a "More Details" button. This button will redirect the user to a new page containing all data related to the dataset.

 ![alt text](./media/PostFixture.png "Title")


### Saved Searches
Many users perform similar searches every day for the same or similar data, the McQuilling database is actively growing because of the processes established by its maintainers and because of its users' activity. For this reason, the ability to save a previous search is one of the McQuilling Search Clients' core features.

The steps to save a search are simple:
* Perform a search
* Wait for the Search to be returned
* Locate the Save Search button located in the top left corner 

 ![alt text](./media/SavedSearch.png "Title")

* Click the button and enter the name of the Search
* Click Ok

 ![alt text](./media/NewSavedSearch.png "Title")

### Accessing Saved Searches and Records
To access the search that was saved, navigate to the top left-hand corner of the dashboard. A username should be displayed in the corner, clicking on the username will open a side drawer with recently saved items as well as recently viewed data.

![alt text](./media/UserBar.png "Title")

Once this screen is reached, a user must then select which data they wish to view and it will be rendered on the screen. This feature can be used on any relevant search with any data type.

### Sorting Searches (Fixtures)
Searches can be sorted and returned in two ways; alphabetical or by Charter Party date. When a search is originally performed it is sorted alphabetically, to rerender the current search to chronological ordering simply click the sort button located at the top left of every search. Then select Charter Party date, if you wish to switch it back merely select the other option and the search will re-render.

## FAQ:
### I can not find the data point I am looking for.
If you can not find a data point from the search client and you are sure your query is formatted correctly; a permissions error might have occurred. Search Client ensures that data points can only be accessed by individuals with the correct desk and credentials. Please submit a help desk ticket to rectify this error.

### I need a more complete look at the data associated with the data point, where can I find that?
Each data point returned by the  McQuilling Search Client has a dedicated page with more information. Just find the data point from the search bar and click "More Details". A new window will appear, with an in-depth presentation of the data associated with the data point. Additionally that link and saved as a reference in the future; just in incase the data were to be updated.
