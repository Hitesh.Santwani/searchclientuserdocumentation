# Vessel Application 
Each data point has a corresponding application designed for a complete assessment of all data related to it. The Vessels data is divided into three sections:

* Vessel Details
* Documents
* Vessel Emails

To navigate to a section, locate the hamburger icon in the top left corner. Once clicked a side drawer module will appear, and each section will have a link displayed in this module. 

![alt text](./media/VesselHamburger.png "Title")

By simply clicking on the name of the section you wish to navigate to, the page then redirects to the corresponding section.

## Vessel Details
The Vessel Details section of the application is the first section to render when the page is loaded, this section contains the majority of the data related to the vessel. This data is grouped by relevance and importance. 
### Vessel Position         
To the majority of our users, vessel position is one of the most important pieces of information. The MLISA database collects AIS position data daily, using this information we can create a history of the ship's position. Once the application loads, if vessel position history is available a map will load at the top of the application. A red dot will appear near the center of the map, this is the vessel's current position. The map may have other markers such as yellow dots, these yellow dots signify a previous AIS position that was recorded. 

![alt text](./media/VesselPosition.png "Title")

This map also has features powered by OpenStreetMap and OpenSeaMap. If a user were to zoom in, they would see features such as port, anchor berth, beacon towers, and buoy markers. A complete list of markers and their means can be found on the [openstreetmap wiki](https://wiki.openstreetmap.org/wiki/Category:Openseamap_Symbols); there are over 200 different marine-based markers associated with the OpenSeasMap.

### Vessel Details
The next card displayed within the Vessel Details section contains some of the vessel's descriptive details; such as IMO number, vessel type, sub-type, dimensions, and status. The fields visible are based on the MLISA desktop client; to recommend fields that would be helpful to the McQuilling team's workflow, please submit a ticket to the McQuilling help desk.

![alt text](./media/VesselDetails.png "Title")

### Companies
A vessel may have several companies involved with its operation and voyage, a list consisting of the registered and commercial owner's primary operator and effective control will be rendered below the vessel details card. Each one of these companies has a complete page dedicated to their company's information; to view these pages simply click on the company of interest and a separate page will render the company's information. 

![alt text](./media/VesselCompanies.png "Title")

### Build
A vessel's build details contain all of a vessel's specifications that were not listed in the Vessel Details card above. The vessel's build year, country, and builder are all displayed in this card; as well as some of the vessel's more descriptive traits like the number of cargo tanks and its engine designation. The fields visible are based on the MLISA desktop client; to recommend fields that would be helpful to the McQuilling team's workflow, please submit a ticket to the McQuilling help desk.

![alt text](./media/VesselBuild.png "Title")

### Vessel Commerical Information
The final card in the Vessel Details section contains 4 separate sections separated by tabs. Each tab contains different types of commercial information on the vessel. The first tab contains commercial information received from external and internal sources. Next are all of the vessel sale transactions; sale price, sale notes as well the parties involved in the transaction are all listed. Finally, Post Fixture and Market Fixture list all commercial voyages performed by the vessel. These are almost identical tabular sections, Post Fixtures differ in only one way; each Post Fixture listed is a link to an internal Post Fixture page. Both of these sections may return many fixtures linked with the vessel, for this reason, the data has been separated into pages. To navigate these pages; use the buttons found at the bottom of the card labeled "Previous" and "Next". Also located at the bottom of the card is located the current page and total pages associated with the data set.
#### __Vessel Commerical Information__
![alt text](./media/VesselCommericalInfo.png "Title")
### __Vessel Sales__
![alt text](./media/VesselSales.png "Title")
### __Vessel Post Fixtures__
![alt text](./media/VesselPostFixtures.png "Title")
### __Vessel Market Fixtures__
![alt text](./media/VesselMarketFixtures.png "Title")

## Attachments
A vessel can have serval documents related to it, these documents can range from sales, build and voyage information. For this reason, McQuilling Data Services have created 4 Folders associated with every vessel; Broking Document, Full TC Description, SNP Document, and Miscellaneous Folder. To navigate to each folder, click on the folder name in the header of the Attachment section. A drop-down list with all available folders will appear, then select the required folder and the page will render a list of attachments in the selected folder.

![alt text](./media/VesselAttachmentsFolder.png "Title")

Some vessels have many attachments in each folder and sometimes finding a particular document becomes difficult to find. To sort the documents in a more manageable fashion; click on the Sort button located in the header of the application. A drop-down will appear with all of the current sort options; currently chronological and alphabetical ordering are currently available. Select the preferred sort method and the attachment will rerender in the preferred order.

![alt text](./media/VesselAttachmentsSort.png "Title")

To view documents attached to a vessel, navigate to the location of the document and click on it. Since Microsoft's proprietary file types can not be rendered without their software, Microsoft file types will be downloaded.(_doc,docx,xlxs,etc._) All other file types will appear on screen within a pop-up rendered by the application, files can not be edited on this platform. To close the pop up simply click anywhere 

![alt text](./media/VesselAttachmentsView.png "Title")

