# Post Fixture Application 
Each data point has a corresponding application designed for a complete assessment of all data related to it. Since Fixtures are a rather complex data point; their application is quite large and contains multiple pages and sections. Those sections are:

* Fixture Details
* Documents
* Itinerary History
* Itinerary Updates
* Fixture Updates
* Fixture Emails

To navigate to each section, locate the hamburger icon in the top left corner. Once clicked a side drawer module will appear, and each section will have a link displayed in this module. 

![alt text](./media/PostFixtureNav.png "Title")

By simply clicking on the name of the section you wish to navigate to, the page then redirects to the corresponding section.

## Fixture Details
When the page initially loads; a user will be directed to the Fixture Details section of the application. This view contains the majority of the logistical data about the Fixture, the page is broken down into 6 sub-sections. (Position, Last Itinerary Update, Itinerary Items, Contact Details, and Claim Details)
### Fixture Detail Header
At the top of the Fixture Details page the Fixture's Vessel, Fixture Number, and Charter Party Date are displayed. Additionally, the Fixture status is also displayed below with a status bar, as the Fixture progresses the bar will slowly fill in the faded area.

![alt text](./media/PostFixtureHeader.png "Title")

### Fixture Vessel Position

McQuilling's logistical database tracks all, if not most of the vessels in the McQuilling fleet daily. Since this data is collected, McQuilling can provide a daily update on most Fixtures. On the map, yellow dots represent the recorded position of the vessel during the course of the voyage. Two larger dots will appear on the screen. One is green, which signifies where the voyage starts and one is red, which signifies the end of the voyage.

![alt text](./media/FixturePosition.png "Title")

### Post Fixture Details

The Post Fixture Details module appears directly below the positional data interface. This area contains two types of information:
* Fixture Data, such as the Vessel's name, Fixture Type, Charter's name, Cargo Type, etc. 
* Modification History, which contains all changes applied to Fixture 

*Vessel name, Charter's name, and Effective Control name can be clicked to open a separate application about that data point

![alt text](./media/PostFixtureDetails.png "Title")

Additionally below, there are two buttons with lend some extra functionality. The Position button will direct the user back to the positional data rendered above and the Edit button will direct the user to the Fixture Updates page.

### Last Intinerary Update & Itinerary Items
![alt text](./media/ItineraryDetails.png "Title")

This module displays the last itinerary item added, each update contains the user code of the user who posted the update and the timestamp when the update was posted. Status, description, and next and last port are also displayed in the module. Two buttons are located in the lower right-hand corner of the module, the Itinerary History button will direct the user to the Itinerary History page of the document and the Confirm button will confirm the last itinerary update with the user's code and timestamp of the confirmation.

*Below this module is a full list of all of the itinerary items that are currently valid on the Fixture
### Claims Details
The last module on the Fixture Detail page is the Claims Details module, this will contain a list of all claims related to the queried fixture. Each claim will state, what type of claim it is, what was the last action and what date that action was made. Additionally, each claim can be opened individually since it's one of the 5 data sets that are queriable, by clicking on the claim a new window will be opened with all of the claims information.

## Fixture Emails
A large percentage of the McQuilling workflow is performed by email; updates, documentation, and all forms of communication are sent via email. This can become very cumbersome to locate an email about a particular fixture when there are many emails incoming every day. McQuilling has developed an email filing system based on Front's tagging system.
### Emails
Emails are separated into a folder structure, these folders are designated for each Fixture. Once a folder is selected, all of the emails related to that folder will be displayed in a column. Each item can be toggled to reveal more information such as the user code of the employee who received or sent the email, the subject, and filed date. At the base of the revealed container are three buttons; one to move the email to a different folder, one to edit the description of the email, and one to view the email in front.

![alt text](./media/EmailDetail.png "Title")
### Folders
When a Fixture is created it is assigned 4 generic folders; Voyage Ops, Charter Docs, Claims, and Accounting. To select a folder, locate the Folder button within the header of the Fixture Email section. Once the button is clicked a list of Folders will be displayed, click on the desired folder and its contents will be rendered below.
![alt text](./media/EmailHeader.png "Title")

![alt text](./media/FolderSelection.png "Title")
Also if a user believes that none of these pre-generated folders satisfies their requirements, a new folder can be created by clicking the New Folder button. A prompt will ask the user for a name for the new folder; once the folder is entered, click ok and the page will re-render with the new folder added. 

### Folder Search
Some folders may contain multiple pages of tagged emails, and a navigation prompt in the bottom right of the header displays the total number of pages returned. Instead of manually searching for the email page by page, a user can enter the subject or description of the email in the input labeled Folder Search. This will perform a query on all the contents of the folder the user is currently in.

### Advanced Search
Some users have grown accustomed to using the Front system interface, the Advanced Search button allows the user to open Front with a pre-filtered selection of the selected Fixtures Emails. As long as the emails are appropriately tagged, they will appear in this search.

### Filing Emails
This section of the application has many great features; like viewing emails and creating folders but its main functionality comes from its ability to file emails to a Fixture's folder easily. To file an email, first, navigate to a folder within the application where the email will be filed too. Then open the Front email client, and find the email that needs to be filed. Once both are acquired, just drag the email from the Front client to the section of the page where the other emails are rendered. The Screen will darken, and a cloud icon and text stating which folder the email is being filed to will appear. Drop the email here, and a secondary prompt will appear asking a user to confirm; once confirmed the page will re-render with the filed email. The email will also now appear in the Advanced Search described above.

![alt text](./media/EmailFiling.png "Title")
## Documents

Many types of documents are used within the lifespan of a Fixture; Q88, contracts, and vessel images are just a few. The Documents section of the applications makes it quite easy to not only view the documents attached to Fixture but to upload documents to a Fixture. To view the attached documents on a Fixture, simply navigate to the document section and click on the desired document. For document types such as pdf, HTML, and standard image types the document will appear in the browser; for all other document file types, a download will occur shortly after the items are clicked.
### List of attached Documents
![alt text](./media/FixtureDocuments.png "Title")
### Rendered Document
![alt text](./media/rendered_document.png "Title")

### Uploading Document to Fixture
Attaching a document to a Fixture is even simpler than rendering a document, a user must just drag and drop a file onto the document section of the application. Once dropped a prompt will ask for the name of the file, just in case the file name does not match its description. Once the user is satisfied with the name of the document; click ok to confirm the action. The document will be added to the Fixture and can be rendered like the other documents.

![alt text](./media/Document_DND.png "Title")

## Itinerary History
Itinerary History is comprised of two major sections, reporting the most recent itinerary update to occur on a Fixture and reporting a history of all recorded itinerary updates to occur. Updates are listed in descending chronological order and have similar fields to the Last Itinerary Update module in the Fixture Detail section of the application. This module also contains two buttons, an Update button which will direct you to the Itinerary update section of the application, and a Confirm button which confirms the last itinerary update with the user's code and current timestamp.

## Itinerary Updates
![alt text](./media/Itin_Update.png "Title")
The Itinerary Update section makes it possible to make itinerary updates from any web browser. Updates are made in an identical way as MLISA, enter details about the existing or new itinerary items. If a new item is detected, a confirmation prompt will appear; it will ask you to correctly position the added itinerary item. Once the user is done updating the Fixtures itinerary; click submit and the Fixture will reload with the uploaded itinerary item.

## Fixture Updates
The Fixture Update section of the application lets the user perform meaningful updates to the Fixture record. Fields such as Fixture Number, Status, Desk, User Code, and Company details can all be changed from this one screen. A user must enter the details they wish to change and simply press the submit button, the application will redirect to the Fixture Details page with the newly updated information.
![alt text](./media/FixtureUpdates.png "Title")

